const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const ctxDb = require("./Database/hostContext");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/get/resource/memory", async (req, res) =>{
    const hosts = await ctxDb.getAll();
    res.status(200).json({ hosts });
});

app.listen(5001, () => console.log("Server is running on port 5001"));