const os = require("os");
const EventEmmitter = require("events");
const ctxDb = require("../Database/hostContext");
const settings = require("../settings.json");
class AgentEvent extends EventEmmitter{}
var emmiter = new AgentEvent();

var emitterOptions ={
    initTime: 0,
    endTime: 0
};

function connect(){
    let currentDate =  new Date();
    emitterOptions.initTime = currentDate,
    emitterOptions.endTime = new Date(currentDate.getTime() + settings.MIN_DISCONNECT * 60000);
}

function disconnect(){
    console.log(emitterOptions.endTime);
    if(new Date() >= emitterOptions.endTime){
        emmiter.removeListener("addMetric", addMetric);
        emmiter.removeListener("removeMetric", removeMetric);
        console.log("disconnected!");
    }
}

async function addMetric(){
    let hostname = os.hostname();
    let freemem = os.freemem();
    let totalmem = os.totalmem();
    let host = {
        "hostname": hostname,
        "total_memory": totalmem,
        "free_memory": freemem,
        "created": new Date()
    };

    var result = await ctxDb.insertHost(host);
    console.log(`addMetric ${result}`);
}

function removeMetric(){
    console.log("removeMetric");
}

emmiter
.prependOnceListener("connect", connect)
.on("disconnect", disconnect)
.on("addMetric", addMetric)
.on("removeMetric",removeMetric);


setInterval(() => {
    emmiter.emit("connect");
    emmiter.emit("disconnect");
    emmiter.emit("addMetric");
}, 1000);