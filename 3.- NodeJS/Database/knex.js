const knex = require("knex");

const connectionKnex = knex({
    client: "sqlite3",
    connection:{
        filename:"agent.db"
    }
});

module.exports = connectionKnex;