const knex = require("./knex");

function insertHost(host){
    return knex("host").insert(host);
}

function getAll(){
    return knex("host").select("*");
}

module.exports = {
    insertHost,
    getAll
}