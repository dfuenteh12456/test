CREATE TABLE bank (
	id  integer PRIMARY KEY AUTOINCREMENT,
    code     char(10)    NOT NULL,
    name     varchar(50)    NOT NULL        
);

INSERT INTO bank (code, name) VALUES('0000000001', 'Banco Falabella');
INSERT INTO bank (code, name) VALUES('0000000002', 'Banco Santander');
INSERT INTO bank (code, name) VALUES('0000000003', 'Banco de Chile');
INSERT INTO bank (code, name) VALUES('0000000004', 'Banco Estado');


CREATE TABLE branch_office (
    id integer PRIMARY KEY AUTOINCREMENT,
    code     char(10)    NOT NULL,
    address     varchar(50)    NOT NULL,
	fk_bank_id integer NOT NULL,
	CONSTRAINT fk_bank_branch_office   FOREIGN KEY (fk_bank_id) REFERENCES bank(id)
);

INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_fal_1', 'Las Amapolas 2354', (select id from bank where code = '0000000001'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_fal_3', 'Los Poetas 1232', (select id from bank where code = '0000000001'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_fal_2', 'Nicanor Parra 3411', (select id from bank where code = '0000000001'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_san_1', 'Los Conquistadores 34', (select id from bank where code = '0000000002'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_san_2', 'Blanco Encalada 378', (select id from bank where code = '0000000002'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_chi_1', 'Alameda 3456', (select id from bank where code = '0000000003'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_est_1', 'AV. estación 456', (select id from bank where code = '0000000004'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_est_2', 'Pelantaro 345', (select id from bank where code = '0000000004'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_est_3', 'Caupolican 4567', (select id from bank where code = '0000000004'));
INSERT INTO branch_office (code, address, fk_bank_id) VALUES('suc_est_4', 'AV. Alemania 45', (select id from bank where code = '0000000004'));



CREATE TABLE user (
    id  integer PRIMARY KEY AUTOINCREMENT,
    rut     char(10)    NOT NULL,
    name     varchar(100)    NOT NULL,
	lastname varchar(100)  NOT NULL,
	age integer NOT NULL,
	CONSTRAINT uk_rut  UNIQUE(rut)
);

INSERT INTO user (rut, name,lastname, age) VALUES ('174991316', 'Daniel', 'Fuentes', 32)     ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174567891', 'Estaban','Dido', 33)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174934567', 'Armando','Casas', 21)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('196789990', 'Pedro', ' Perez', 45)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('89767567', 'Juan', 'Iturriaga', 23)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('44567788', 'Roberto','Mendoza', 21)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('245675432', 'Cristian','Perez', 57)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('161234123', 'Michael','Bustos', 87)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('134532212', 'Tatiana','Medel', 67)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991311', 'Juana','Abarzua', 33)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991312', 'Pedro', 'Picapiedra', 36)   ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991313', 'Maria','Tobar', 37)         ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991314', 'Raquel','Lewinsky', 31)     ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991315', 'Ariel','Riquelme', 38)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991317', 'Fiodor','Dostoievsky', 39)  ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991318', 'Charles','Bukowsky', 42)    ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991319', 'Jhon','Fante', 41)          ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174991310', 'Jhon','Steinbeck', 49)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('124991516', 'Heran', 'Rivera', 48)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('134991416', 'Jose', 'Donoso', 47)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('154991216', 'Jose','Sepulveda', 48)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('164991116', 'Leon','Tolstoi', 54)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('184991116', 'Rocio','Stevens', 35)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('124991116', 'Pedro','Pascal', 32)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('24991116', 'Ramon','Gutierrez', 56)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('74991116', 'Alan','Brito', 53)           ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174901116', 'Lucrecia','Salvador', 45)   ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174911116', 'Clemencia','Lincopan', 32)  ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174921116', 'Pablo','Milagro', 48)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174931116', 'Ruben','Pinto', 76)         ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174941116', 'Ramon','Rodriguez', 67)     ;

INSERT INTO user (rut, name,lastname, age) VALUES ('174951116', 'Ricardo','Lagos', 66)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174961116', 'Renato','Aliaga', 34)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174971116', 'Roman','Restrepo', 56)      ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174981116', 'Marcos','Ibañez', 76)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('174591116', 'Armando','Balmaceda', 21)   ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162321', 'Clemente','Quiroz', 55)     ;

INSERT INTO user (rut, name,lastname, age) VALUES ('171162322', 'David','Ibañez', 31)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162323', 'Dana','Mariajel', 32)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162324', 'Doralisa','Fuentes', 44)    ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162325', 'Dina','Alonso', 55)         ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162326', 'Fernando','Perez', 22)      ;

INSERT INTO user (rut, name,lastname, age) VALUES ('171162327', 'Felipe','Estay', 25)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162328', 'Felo','Relo', 43)           ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162332', 'Florencia','Marin', 44)     ;
INSERT INTO user (rut, name,lastname, age) VALUES ('171162331', 'Felipe','Figueroa', 45)     ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175511116', 'Francia','Belmar', 46)      ;

INSERT INTO user (rut, name,lastname, age) VALUES ('175521116', 'Karen','Tepito', 47)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175531116', 'Luis','Carril', 48)         ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175541116', 'Lupe','De Vega', 49)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175551116', 'Sancho','Panza', 34)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175561116', 'Monserrat','Heles', 35)     ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175571116', 'Carmen','Fuentes', 36)      ;

INSERT INTO user (rut, name,lastname, age) VALUES ('175581116', 'Crisol','Castro', 37)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('175591116', 'Alejandro','Quiroz', 38)    ;
INSERT INTO user (rut, name,lastname, age) VALUES ('191231116', 'Ximena','Araujo', 39)       ;
INSERT INTO user (rut, name,lastname, age) VALUES ('192311116', 'Zulema','Args', 40)         ;
INSERT INTO user (rut, name,lastname, age) VALUES ('196781116', 'Tony','Montana', 41)        ;
INSERT INTO user (rut, name,lastname, age) VALUES ('198761116', 'Pedro','Leon', 42)          ;
INSERT INTO user (rut, name,lastname, age) VALUES ('194561116', 'ALberto','Fuentes', 32)     ;


CREATE TABLE branch_office_employee (
    fk_branch_office_id integer NOT NULL,
    fk_user_id     integer    NOT NULL,
	CONSTRAINT pk_branch_office_employee  PRIMARY KEY (fk_branch_office_id, fk_user_id),
	CONSTRAINT fk_branch_office_employee_user   FOREIGN KEY (fk_user_id) REFERENCES user(id),
	CONSTRAINT fk_branch_office_employee_branch_office  FOREIGN KEY (fk_branch_office_id) REFERENCES branch_office(id)
);


insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '174991316'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '174567891'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '174934567'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '196789990'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '89767567')) ;
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '44567788')) ;
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '245675432'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '161234123'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '134532212'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '174991311'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '174991312'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '174991313'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '174991314'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '174991315'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '174991317'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '174991318'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '174991319'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '174991310'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '124991516'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '134991416'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '154991216'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '164991116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '184991116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '124991116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '24991116')) ;
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '74991116')) ;
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '174901116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '174911116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '174921116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '174931116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '174941116'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '174951116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '174961116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '174971116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '174981116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '174591116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '171162321'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '171162322'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '171162323'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '171162324'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '171162325'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '171162326'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '171162327'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '171162328'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '171162332'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '171162331'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '175511116'));
																																												   
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175521116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175531116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175541116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175551116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175561116'));
insert into branch_office_employee (fk_branch_office_id, fk_user_id) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175571116'));


CREATE TABLE branch_office_security(
    fk_branch_office_id integer NOT NULL,
    fk_user_id  integer    NOT NULL,
	hiring_date datetime NOT NULL,
	CONSTRAINT pk_branch_office_security  PRIMARY KEY (fk_branch_office_id),
	CONSTRAINT fk_branch_office_security_user   FOREIGN KEY (fk_user_id) REFERENCES user(id),
	CONSTRAINT fk_branch_office_security_branch_office  FOREIGN KEY (fk_branch_office_id) REFERENCES branch_office(id)
);

insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_fal_1'), (select id from user where rut = '175581116'), (select DateTime('Now', 'LocalTime', '-12 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_fal_2'), (select id from user where rut = '175591116'), (select DateTime('Now', 'LocalTime', '-25 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_fal_3'), (select id from user where rut = '191231116'), (select DateTime('Now', 'LocalTime', '-45 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_san_1'), (select id from user where rut = '192311116'), (select DateTime('Now', 'LocalTime', '-63 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_san_2'), (select id from user where rut = '196781116'), (select DateTime('Now', 'LocalTime', '-547 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_chi_1'), (select id from user where rut = '198761116'), (select DateTime('Now', 'LocalTime', '-221 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_est_1'), (select id from user where rut = '194561116'), (select DateTime('Now', 'LocalTime', '-321 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_est_2'), (select id from user where rut = '175581116'), (select DateTime('Now', 'LocalTime', '-124 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_est_3'), (select id from user where rut = '175581116'), (select DateTime('Now', 'LocalTime', '-897 Day')));
insert into branch_office_security (fk_branch_office_id, fk_user_id, hiring_date) values((select id from branch_office where code = 'suc_est_4'), (select id from user where rut = '175581116'), (select DateTime('Now', 'LocalTime', '-456 Day')));



CREATE TABLE login(
	fk_user_id integer NOT NULL,
    password NVARCHAR   NOT NULL,
	CONSTRAINT pk_login  PRIMARY KEY (fk_user_id),
	CONSTRAINT fk_login_user   FOREIGN KEY (fk_user_id) REFERENCES user(id)
);

insert into login (fk_user_id, password)  values((select id from user where rut = '175521116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175531116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175541116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175551116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175561116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175571116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162327'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162328'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162332'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162331'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '175511116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162322'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162323'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162324'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162325'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '171162326'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '174901116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '174911116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '174921116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '174931116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');
insert into login (fk_user_id, password)  values((select id from user where rut = '174941116'),'ZXN0byBlcyB1bmFjb250cmFzZcOxYSBzdXBlciBpbnNlZ3VyYQ==');

CREATE TABLE incident_type(
    id integer PRIMARY KEY AUTOINCREMENT,
    detail     VARCHAR(50)    NOT NULL
);

insert into incident_type (detail) values ('Llamada a Carabineros');
insert into incident_type (detail) values ('Otros riesgos');

CREATE TABLE incident(
    id integer PRIMARY KEY AUTOINCREMENT,
    fk_incident_type_id  integer  NOT NULL,
    fk_user_id     integer  NOT NULL,
	fk_branch_office_id integer NOT NULL,
	created DATETIME NOT NULL,
	comment VARCHAR(250) NULL,
	CONSTRAINT fk_incident_user   FOREIGN KEY (fk_user_id) REFERENCES user(id),
	CONSTRAINT fk_incident_incident_type   FOREIGN KEY (fk_incident_type_id) REFERENCES incident_type(id)
	CONSTRAINT fk_incident_branch_office  FOREIGN KEY (fk_branch_office_id) REFERENCES branch_office(id)

);

insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Llamada a Carabineros'), (select id from user where rut = '175581116'), (select id from branch_office where code = 'suc_fal_1'), (select datetime()), 'Intento de robo');
insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Otros riesgos'), (select id from user where rut = '192311116'), (select id from branch_office where code = 'suc_fal_3'), (select datetime()), 'Cámara principal desactivada');
insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Otros riesgos'), (select id from user where rut = '175581116'), (select id from branch_office where code = 'suc_chi_1'), (select datetime()), 'Cajas abiertas');
insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Otros riesgos'), (select id from user where rut = '192311116'), (select id from branch_office where code = 'suc_fal_1'), (select datetime()), 'Personas extrañas en el lugar');
insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Otros riesgos'), (select id from user where rut = '175581116'), (select id from branch_office where code = 'suc_fal_2'), (select datetime()), 'Interrupción de suministro electrico');
insert into incident (fk_incident_type_id, fk_user_id, fk_branch_office_id, created, comment) values((select id from incident_type where detail = 'Llamada a Carabineros'), (select id from user where rut = '192311116'), (select id from branch_office where code = 'suc_est_4'), (select datetime()), 'Disturbios en cercanías de la sucursal');

CREATE TABLE after_hour_register(
    id integer PRIMARY KEY AUTOINCREMENT,
    fk_user_id integer NOT NULL,
    arrival DATETIME    NOT NULL,
	departure DATETIME NULL,
	CONSTRAINT fk_after_hour_register_user   FOREIGN KEY (fk_user_id) REFERENCES user(id)
);


insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '174991319'), (select DateTime('Now', 'LocalTime', '-10 Day')),(select Datetime('Now', 'LocalTime', '-10 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '174991310'), (select DateTime('Now', 'LocalTime', '-9 Day')),(select Datetime('Now', 'LocalTime', '-9 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '124991516'), (select DateTime('Now', 'LocalTime', '-8 Day')),(select Datetime('Now', 'LocalTime', '-8 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '134991416'), (select DateTime('Now', 'LocalTime', '-7 Day')),(select Datetime('Now', 'LocalTime', '-7 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '154991216'), (select DateTime('Now', 'LocalTime', '-6 Day')),(select Datetime('Now', 'LocalTime', '-6 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '245675432'), (select DateTime('Now', 'LocalTime', '-5 Day')),(select Datetime('Now', 'LocalTime', '-5 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '161234123'), (select DateTime('Now', 'LocalTime', '-4 Day')),(select Datetime('Now', 'LocalTime', '-4 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '134532212'), (select DateTime('Now', 'LocalTime', '-3 Day')),(select Datetime('Now', 'LocalTime', '-3 Day')));
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '174991311'), (select DateTime('Now', 'LocalTime', '-1 Hour')),null);
insert into after_hour_register (fk_user_id, arrival, departure)  values((select id from user where rut = '174991312'), (select DateTime('Now', 'LocalTime', '-1 Hour')),null);





