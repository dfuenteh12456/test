-- Selección de bancos con sus respectivas sucursales
select b.name, bo.code, bo.address from bank b
inner join branch_office bo on b.id = bo.fk_bank_id;

-- Selección de personal de banco con edad mayor a 30
select u.name as user_name, u.age, b.name as bank_name, bo.code as branch_office_code from bank b
inner join branch_office bo on b.id = bo.fk_bank_id
inner join branch_office_employee boe on bo.id = boe.fk_branch_office_id
inner join user u on boe.fk_user_id = u.id
where u.age > 30
order by u.age desc;

-- Selección de los incidentes reportados
select it.detail, i.comment, b.name as bank_name, bo.code branch_office_code, i.created, u.name user_name_reporter  from incident i
inner join incident_type it on i.fk_incident_type_id = it.id
inner join user u on i.fk_user_id = u.id
inner join branch_office bo on i.fk_branch_office_id = bo.id
inner join bank b on bo.fk_bank_id = b.id;

-- Detalle de bancos cuya cantidad de sucursales sean mayor a 1
select bo.fk_bank_id as bank_id, count(bo.id) branch_office_qauntity, b.name bank_name
from branch_office bo
inner join bank b on bo.fk_bank_id = b.id
group by bo.fk_bank_id
having count(bo.id) > 1;


-- Aplicación de case y concat
select 
case it.detail 
	when 'Llamada a Carabineros' 
		then 'Petición de refuerzos'
	else 'Incidentes internos'
end incident_type,
i.comment,
i.created,
(u.name || ' ' || u.lastname) reporter
from incident i
inner join incident_type it on i.fk_incident_type_id = it.id
inner join user u on i.fk_user_id = u.id;
